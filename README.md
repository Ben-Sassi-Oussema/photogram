## :pencil: Project Description
This is my interpretation of Instagram Application.

# Setup to run it on you Computer
You need the .NET Core 3.1 SDK (Download: https://dotnet.microsoft.com/download) and the Microsoft Sql Server (Download: https://www.microsoft.com/en-au/sql-server/sql-server-downloads *Free Developer Version)

- When you Run the project it will generate a sql database without password just login with your server :localhost
- The default connection in ./appsettings.json 


# Features
- Login/Register System
- Comments
- Posts
- Like Posts
- Follow Users
  
 # Sources :
 - https://github.com/YordanDobrev97/Gangstagram
 - https://github.com/kcrnac/ng-instagram/tree/master/backend

# Images
 
  <img src="./Images/login.PNG" width="700">

  <hr />
  <img src="./Images/register.PNG" width="700">

  <hr />

  <img src="./Images/index.PNG" width="700">
  
  <hr />

  <img src="./Images/Post.PNG" width="700">
  
  <hr />


  <img src="./Images/Profil.PNG" width="700">
  
  <hr />
 <img src="./Images/comment-Like.PNG " width="700">
  
  <hr />

  <img src="./Images/like.PNG" width="700">
